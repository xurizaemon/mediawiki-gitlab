# Gitlab integration for MediaWiki

## Status

Not yet released, prototype only. Please see [issue queue](https://gitlab.com/xurizaemon/mediawiki-gitlab/-/issues) for status.

## Installation

This project is not yet released. There are some setup steps first.

1. `composer config repositories.mediawiki-gitlab vcs git@gitlab.com:xurizaemon/mediawiki-gitlab.git`
2. `composer requre xurizaemon/mediawiki-gitlab`

## Configuration

Obtain an access token or tokens for the Gitlab(s) you wish to reference. More than one Gitlab host or URL realm is supported. You may use Group, Project or Personal access tokens.

Add `$wgGitlabAccessTokens` array to your `LocalSettings.php` with the tokens, keyed by the URL prefix the access token is valid for.

```php
// First match wins, order most specific to top.
$wgGitlabAccessTokens = [
    'https://gitlab.example.org/drupal' => 'glpat-asdf-qwertyuiop-abcd',
    'https://gitlab.example.org' => 'glpat-plokijuhygtfrdeswaqa',
    'https://gitlab.com/behat-chrome' => 'glpat-zxcvbnmzxcvbnmzxcvbm',
    'https://gitlab.com' => 'glpat-yyyyyyyyyyyyyyyyyyyy',
];
```

## Features

Turn Gitlab URLs into labelled references.

![input](assets/input.png)

![output](assets/output.png)