<?php
/**
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * @file
 */

namespace MediaWiki\Extension\Gitlab;

use MediaWiki\MediaWikiServices;
use MediaWiki\Hook\ParserFirstCallInitHook;
use Gitlab\Client;

class Hooks implements ParserFirstCallInitHook {

    /**
     * Bind our Gitlab parser to the {{#gitlab}} template.
     *
     * @param Mediawiki\Parser $parser
     */
    public function onParserFirstCallInit( $parser )
    {
        $parser->setFunctionHook( 'gitlab', [ self::class, 'renderGitlabParser' ] );
    }

    /**
     * Parse a Gitlab function tag.
     *
     * @param \Parser $parser
     * @param string $url
     * @param string $param2
     * @param string $param3
     * @return string
     */
    public static function renderGitlabParser( \Parser $parser, $url = '', $param2 = '', $param3 = '' ) {
        $config = MediaWikiServices::getInstance()->getConfigFactory()->makeConfig( 'Gitlab' );
        $accessTokens = $config->get( 'GitlabAccessTokens' );

        if ($parsed_url = self::parseUrl($url)) {
            foreach ($accessTokens as $key => $value) {
                if (stristr($url, $key)) {
                    $client = new Client();
                    $client->authenticate($value, Client::AUTH_HTTP_TOKEN);
                }
            }

            if (!isset($client)) {
                return "{$url} (Gitlab error: no access token configured for URL)";
            }

            try {
                switch ($parsed_url['entity_type']) {
                    case 'issues':
                        $issue = $client->issues()->show($parsed_url['project_id'], $parsed_url['entity_id']);
                        $output = "<a href='{$issue['web_url']}' class='state-{$issue['state']}'>{$issue['references']['full']}: {$issue['title']}</a> (<span class='state {$issue['state']}'>{$issue['state']}</span>)";
                        return [
                            $output,
                            'noparse' => true,
                            'isHTML' => true,
                        ];

                    case 'merge_requests':
                        $issue = $client->mergeRequests()->show($parsed_url['project_id'], $parsed_url['entity_id']);
                        $output = "<a href='{$issue['web_url']}' class='state-{$issue['state']}'>{$issue['references']['full']}: {$issue['title']}</a> (<span class='state {$issue['state']}'>{$issue['state']}</span>)";
                        return [
                            $output,
                            'noparse' => true,
                            'isHTML' => true,
                        ];

                    default:
                        $output = "{$url} (Gitlab: URL could not be parsed)";
                }
            }
            catch (\Exception $e) {
                return "{$url} (Gitlab error: {$e->getMessage()})";
            }
        }

        return $output;
    }

    /**
     * @param $url
     */
    public static function parseUrl($url) {
        $parsed = parse_url($url);
        // scheme, host, path
        if ($gitlabPath = explode('/-/', $parsed['path'])) {
            $entity = explode('/', $gitlabPath[1]);
            $parsed['entity_type'] = $entity[0];
            $parsed['entity_id'] = $entity[1];
            $parsed['project_id'] = trim($gitlabPath[0], '/');
        }
        return $parsed;
    }

}
